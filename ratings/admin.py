from django.contrib import admin

from ratings.models import *

class RestaurantAdmin(admin.ModelAdmin):
	list_display = ('name', 'desc','lat', 'lon',)

class ReviewAdmin(admin.ModelAdmin):
	list_display = ('name', 'restaurant', 'rating',)

admin.site.register(Restaurant, RestaurantAdmin)
admin.site.register(Review, ReviewAdmin)


