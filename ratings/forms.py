from django import forms
from django.forms import ModelForm
from django.contrib.admin import widgets
from ratings.models import *

class ReviewForm(ModelForm):
	class Meta:
		model = Review
		exclude = ('name','restaurant',)
