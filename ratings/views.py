# Create your views here.
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.core.urlresolvers import reverse, reverse_lazy
from registration.backends.simple.views import RegistrationView
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.models import User
from django.utils import timezone
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.utils.decorators import method_decorator
from django.views.generic import ListView, DetailView, UpdateView, DeleteView, CreateView
from endless_pagination.views import AjaxListView

from ratings.models import *
from ratings.forms import *

class RestaurantList(ListView):#possibly paginate this view
	model = Restaurant
	context_object_name = 'restaurants'
	template_name = 'ratings/index.html'

class RestaurantListMap(ListView):
	model = Restaurant
	context_object_name = 'restaurants'
	template_name = 'ratings/map.html'

class RestaurantDetail(DetailView):
	model = Restaurant
	context_object_name = 'restaurant'
	template_name = 'ratings/restaurant_detail.html'

	def get_context_data(self, **kwargs):
		user = self.request.user
		context = super(RestaurantDetail, self).get_context_data(**kwargs)
		context['user'] = user
		return context

class AddReview(CreateView):
	model = Review
	template_name = 'ratings/review_add.html'
	form_class = ReviewForm

	def get_form(self, form_class):
		user = self.request.user
		restaurant = get_object_or_404(Restaurant, pk = self.kwargs['restaurant_id'])
		form = super(AddReview, self).get_form(form_class)
		form.instance.name = user
		form.instance.restaurant = restaurant
		return form

	
	@method_decorator(login_required)
    	def dispatch(self, *args, **kwargs):
        	return super(AddReview, self).dispatch(*args, **kwargs)

class UpdateReview(UpdateView):
	model = Review
	template_name = 'ratings/review_update.html'
	form_class = ReviewForm

	def get_context_data(self, **kwargs):
		user = self.request.user
		review = get_object_or_404(Review, pk = self.kwargs['pk'])
		cur_user = False
		if int(review.name.id) == int(user.id):
			cur_user = True
		context = super(UpdateReview, self).get_context_data(**kwargs)
		context['cur_user'] = cur_user
		return context


	@method_decorator(login_required)
    	def dispatch(self, *args, **kwargs):
        	return super(UpdateReview, self).dispatch(*args, **kwargs)

class DeleteReview(DeleteView):
	model = Review
	success_url = reverse_lazy('ratings:index')

	def get_context_data(self, **kwargs):
		user = self.request.user
		review = get_object_or_404(Review, pk = self.kwargs['pk'])
		cur_user = False
		if int(review.name.id) == int(user.id):
			cur_user = True
		context = super(DeleteReview, self).get_context_data(**kwargs)
		context['cur_user'] = cur_user
		return context

	@method_decorator(login_required)
    	def dispatch(self, *args, **kwargs):
        	return super(DeleteReview, self).dispatch(*args, **kwargs)

class MyRegistrationBackend(RegistrationView):
    def get_success_url(self, request, user):
        return reverse('ratings:index')
