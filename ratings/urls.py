from django.conf.urls import patterns, url

from ratings import views

urlpatterns = patterns('',
	url(r'^$', views.RestaurantList.as_view(), name='index'),
	url(r'^map/$', views.RestaurantListMap.as_view(), name='map'),
	url(r'^restaurant/(?P<pk>\d+)/$', views.RestaurantDetail.as_view(), name='restaurant_detail'),
	url(r'^restaurant/(?P<restaurant_id>\d+)/add_review/$', views.AddReview.as_view(), name='add_review'),
	url(r'^restaurant/(?P<restaurant_id>\d+)/update_review/(?P<pk>\d+)/$', views.UpdateReview.as_view(), name='update_review'),
	url(r'^restaurant/(?P<restaurant_id>\d+)/delete_review/(?P<pk>\d+)/$', views.DeleteReview.as_view(), name='delete_review'),
)
