from django.db import models
from django.core.urlresolvers import reverse
from geopy import geocoders
from django.contrib.auth.models import User
# Create your models here.

class Restaurant(models.Model):
	name = models.CharField(max_length=100)
	address = models.CharField(max_length=100) 
	desc = models.TextField()
	city = models.CharField(max_length=100)
	state = models.CharField(max_length=15)
	zipcode = models.CharField(max_length=25)
	lat = models.CharField(max_length=20, editable=False, null=True, blank=True)
	lon = models.CharField(max_length=20, editable=False, null=True, blank=True)

	def __unicode__(self):
		return self.name

	def save(self, *args, **kwargs):
		addr = self.address
		city = self.city
		state = self.state
		g = geocoders.GoogleV3()
		place, (lat, lon) = g.geocode(str(addr) + " " + str(city) + " " + str(state))
		self.lat = lat
		self.lon = lon
		super(Restaurant, self).save(*args, **kwargs)

	@property
	def reviews(self):
		return Review.objects.all().filter(restaurant=self)
	
	@property
	def avg_rating(self):
		ratings = Review.objects.all().filter(restaurant=self)
		total_points = 0
		num = 0
		for rating in ratings:
			total_points += rating.rating*1.0
			num +=1
		return total_points/num

class Review(models.Model):
	name = models.ForeignKey(User)
	restaurant = models.ForeignKey(Restaurant)
	rating = models.IntegerField(help_text='out of 10')
	comment = models.TextField()

	def __unicode__(self):
		return "%s-%s" % (self.name, self.restaurant)

	def get_absolute_url(self):
		return reverse('ratings:restaurant_detail', args=(self.restaurant.id,))


